﻿using MagWien.PACE.DSS.Metadaten;
using MagWien.PACE.DSS.Personendaten;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;

namespace MagWien.PACE.Xmas.Objects
{
    /// <summary>
    /// Ein Weihnachtsdekorationsobjekt
    /// </summary>
    public class Weihnachtsdekoration
    {
        /// <summary>
        /// Bezeichnung des Antrags
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Optionale Beschreibung
        /// </summary>
        public string Untertitel { get; set; }

        /// <summary>
        /// Eindeutiger Identifier
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Beschreibung des aktuellen Antragsstatus
        /// </summary>
        public string Statustext { get; set; }

        /// <summary>
        /// Maschinenlesbarer Status
        /// </summary>
        public Verfahrensstatus Verfahrensstatus { get; set; }

        /// <summary>
        /// Wann der Antrag eingereicht wurde
        /// </summary>
        public DateTime Erstelldatum { get; set; }

        /// <summary>
        /// Wann der Antrag zuletzt geändert wurde
        /// </summary>
        public DateTime Änderungsdatum { get; set; }

        /// <summary>
        /// Arten der Dekoration
        /// </summary>
        public enum Arten
        {
            Figur,
            Schmuck,
            Christbaum
        }

        /// <summary>
        /// Art der Weihnachtsdekoration
        /// </summary>
        [JsonConverter(typeof(StringEnumConverter))]
        public Arten Art { get; set; }

        /// <summary>
        /// Ob die Deko leuchtet
        /// </summary>
        public bool Leuchtet { get; set; }

        /// <summary>
        /// Ursprünglicher Antragsteller (mit unverschlüsselter bPK des Bereiches BW)
        /// </summary>
        public NatürlichePerson Antragsteller { get; set; }
    }
}
