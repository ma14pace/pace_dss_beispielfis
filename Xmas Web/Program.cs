using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;

namespace MagWien.PACE.Xmas
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            return Host.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration((hostContext, configApp) =>
                {
                    const string appsettings = "appsettings";
                    configApp.SetBasePath(hostContext.HostingEnvironment.ContentRootPath);
                    configApp.AddJsonFile($"{appsettings}.json", optional: false, reloadOnChange: true);
                    configApp.AddJsonFile($"{appsettings}.{hostContext.HostingEnvironment.EnvironmentName}.json",
                    optional: false, reloadOnChange: true);
                    configApp.AddJsonFile($"{appsettings}.{hostContext.HostingEnvironment.EnvironmentName}." +
                    $"{Environment.GetEnvironmentVariable("USERNAME")}.json", optional: true);
                    configApp.AddEnvironmentVariables();
                })
                 .ConfigureLogging((context, logging) =>
                 {
                     logging.ClearProviders();
                 })
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
        }
    }
}
