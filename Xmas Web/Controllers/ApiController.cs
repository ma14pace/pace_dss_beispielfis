﻿using MagWien.PACE.DSS.DatabrokerServices;
using MagWien.PACE.DSS.DatabrokerServices.Interfaces;
using MagWien.PACE.DSS.Datentypen;
using MagWien.PACE.DSS.Metadaten;
using MagWien.PACE.DSS.Personendaten;
using MagWien.PACE.Xmas.Objects;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MagWien.PACE.Xmas.Controllers
{
    /// <summary>
    /// Hauptcontroller für Zugriffe vom Databroker
    /// </summary>
    [ApiController]
    [Route("api/[action]")]
    public class ApiController : ControllerBase
    {
        private readonly IDssConnection _dssConnection;
        private readonly DssSettings _dssSettings;

        public ApiController(IDssConnection dssConnection, DssSettings dssSettings)
        {
            _dssConnection = dssConnection;
            _dssSettings = dssSettings;
        }

        #region Doku_Snippet_Mock_Tabelle_Dekos
        /// <summary>
        /// Mock-Datenbanktabelle für Weihnachtsdekorationen
        /// </summary>
        internal static List<Weihnachtsdekoration> Dekos = new List<Weihnachtsdekoration>
        {
            new Weihnachtsdekoration
            {
                Id = new Guid("A64EC059-C705-4592-B55D-44965B080710"),
                Name = "Herzchenlampen im Rathauspark",
                Art = Objects.Weihnachtsdekoration.Arten.Schmuck,
                Leuchtet = true,
                Verfahrensstatus = Verfahrensstatus.Abgeschlossen,
                Statustext = "Genehmigt",
                Erstelldatum = new DateTime(2020, 06,22),
                Änderungsdatum = new DateTime(2020, 06, 22),
                Antragsteller = new NatürlichePerson
                {
                    Vornamen = new [] { "XXXMax" },
                    Nachname = "XXXMuster",
                    Geburtsdatum = new DateTime(1980, 1, 1),
                    Bpk = new Personenkennzeichen("BW", "wNR1XdrB7uDb2m2OzRcqp/9cNnI=")
                }
            },
            new Weihnachtsdekoration
            {
                Id = new Guid("09631861-E2D0-4022-AA57-EDA4060BB17A"),
                Name = "Christbaum am Rathausplatz",
                Art = Objects.Weihnachtsdekoration.Arten.Christbaum,
                Leuchtet = true,
                Verfahrensstatus = Verfahrensstatus.Laufend,
                Statustext = "In Bearbeitung",
                Erstelldatum = new DateTime(2020, 06,22),
                Änderungsdatum = new DateTime(2020, 06, 22),
                Antragsteller = new NatürlichePerson
                {
                    Vornamen = new [] { "XXXTest" },
                    Nachname = "XXXSZR",
                    Geburtsdatum = new DateTime(1960, 4, 4),
                    Bpk = new Personenkennzeichen("BW", "NfSQ7h3E0SM2BL9Kf8agWJnW2zI=")
                }
            }
        };
        #endregion Doku_Snippet_Mock_Tabelle_Dekos

        #region Doku_Snippet_Mock_Tabelle_Protokoll
        /// <summary>
        /// Mock-Datenbanktabelle für historisches Protokoll der Weihneachtsdekorationen
        /// </summary>
        internal static List<(Guid DekoId, IVerfahrensschritt Schritt)> Protokoll =
            new List<(Guid DekoId, IVerfahrensschritt Schritt)>
        {
            (new Guid("A64EC059-C705-4592-B55D-44965B080710"), new ReinTextVerfahrensschritt
                {
                    Erstelldatum = new DateTime(2020, 06,22),
                    VonBürger = true,
                    Text = "Deko beantragt"
                }),
            (new Guid("09631861-E2D0-4022-AA57-EDA4060BB17A"), new ReinTextVerfahrensschritt
                {
                    Erstelldatum = new DateTime(2020, 06,22),
                    VonBürger = true,
                    Text = "Deko beantragt"
                })
        };
        #endregion Doku_Snippet_Mock_Tabelle_Protokoll

        [HttpGet, ActionName(nameof(Test))]
        public string Test()
        {
            return "ok";
        }

        #region Doku_Snippet_Weihnachtsdekoration_GetDaten
        /// <summary>
        /// Databroker-GetDaten-Aufruf für Eintrag "Weihnachtsdekoration"
        /// </summary>
        [HttpPost, ActionName(nameof(Weihnachtsdekoration))]
        public IEnumerable<Weihnachtsdekoration> Weihnachtsdekoration([FromBody] Anfrage anfrage)
        {
            if (anfrage == null)
                throw new ArgumentNullException(nameof(anfrage));

            IEnumerable<Weihnachtsdekoration> result = Dekos;

            // Einschränkung auf Datensatznummern
            if (anfrage.Datensatznummern != null && anfrage.Datensatznummern.Any())
            {
                var datensatznummern = anfrage.Datensatznummern.Select(nr => Guid.Parse(nr));
                result = result.Where(deko => datensatznummern.Contains(deko.Id));
            }
            // Wenn nicht explizit auf Datensatznummer eingeschränkt, dann auf nur genehmigte einschränken
            else
                result = result.Where(deko => deko.Verfahrensstatus == Verfahrensstatus.Abgeschlossen);

            // Einschränkung auf Person
            if (anfrage.Personen != null && anfrage.Personen.Any())
            {
                result = result.Where(deko => anfrage.Personen.Contains(deko.Antragsteller));
            }

            return result;
        }
        #endregion Doku_Snippet_Weihnachtsdekoration_GetDaten

        #region Doku_Snippet_Ablaufsprotokoll_GetDaten
        /// <summary>
        /// Databroker-GetDaten-Aufruf für Eintrag "Ablaufsprotokoll"
        /// </summary>
        [HttpPost, ActionName(nameof(Ablaufsprotokoll))]
        public ActionResult Ablaufsprotokoll([FromBody] Anfrage anfrage)
        {
            if (anfrage == null)
                throw new ArgumentNullException(nameof(anfrage));

            IEnumerable<(Guid DekoId, IVerfahrensschritt Schritt)> result = Protokoll;

            // Einschränkung auf Datensatznummern
            if (anfrage.Datensatznummern != null && anfrage.Datensatznummern.Any())
            {
                var datensatznummern = anfrage.Datensatznummern.Select(nr => Guid.Parse(nr));
                result = result.Where(schritt => datensatznummern.Contains(schritt.DekoId));
            }
            var resultSchritte = result.Select(r => r.Schritt);

            // Richtig serialisieren, "Type" muss mitgegeben werden -
            // also auch hier auf TypeJsonConverter nicht vergessen!
            var serializerSettings = new Newtonsoft.Json.JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore
            };
            serializerSettings.Converters.Add(new VerfahrensschrittJsonConverter());
            return new JsonResult(resultSchritte, serializerSettings);
        }
        #endregion Doku_Snippet_Ablaufsprotokoll_GetDaten

        /// <summary>
        /// Databroker-GetDaten-Aufruf der einfach ein leeres Array liefert
        /// </summary>
        [HttpPost, ActionName(nameof(LeeresArray))]
        public IEnumerable<object> LeeresArray([FromBody] Anfrage _)
        {
            return Array.Empty<object>();
        }
    }
}