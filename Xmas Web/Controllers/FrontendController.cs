﻿using MagWien.PACE.DSS.DatabrokerServices;
using MagWien.PACE.DSS.DatabrokerServices.Interfaces;
using MagWien.PACE.DSS.Datentypen;
using MagWien.PACE.DSS.Metadaten;
using MagWien.PACE.DSS.Personendaten;
using MagWien.PACE.Xmas.Objects;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;
using static MagWien.PACE.Base.AspNet.Http.HeaderExtensions;

namespace MagWien.PACE.Xmas.Controllers
{
    /// <summary>
    /// Controller für unser Frontend
    /// </summary>
    [ApiController]
    [Route("api/[action]")]
    public class FrontendController : ControllerBase
    {
        private readonly IDssConnection _dssConnection;
        private readonly DssSettings _dssSettings;

        public FrontendController(IDssConnection dssConnection, DssSettings dssSettings)
        {
            _dssConnection = dssConnection;
            _dssSettings = dssSettings;
        }

        #region Doku_Snippet_Begruessung
        /// <summary>
        /// Endpunkt für die Begrüßung
        /// </summary>
        [HttpGet, ActionName(nameof(Begrüßung))]
        public async Task<string> Begrüßung()
        {
            try
            {
                var person = await GetPerson().ConfigureAwait(false);
                return $"Hallo, { person.Vornamen.Single()} { person.Nachname}";
            }
            catch (UnauthorizedAccessException ex)
            {
                return "Fehler: " + ex.Message;
            }
        }
        #endregion Doku_Snippet_Begruessung

        #region Doku_Snippet_GetPerson
        /// <summary>
        /// Gibt aktuell eingeloggte Person zurück oder liefert Exception
        /// </summary>
        /// <exception cref="UnauthorizedAccessException">Vorraussetzungen fehlgeschlagen</exception>
        private async Task<NatürlichePerson> GetPerson()
        {
            // Bestimmen, ob eingeloggt
            var pvpUser = Request.Headers.GetDomainUserFromHeader();
            if (pvpUser == null || !pvpUser.Domain.Equals("dyn", StringComparison.OrdinalIgnoreCase))
                throw new UnauthorizedAccessException("Nicht eingeloggt.");

            // bPK bestimmen
            var accountApi = _dssConnection.GetAccountAPI(new DssAuthModeFis(_dssSettings));
            var accountResponse = await accountApi.GetAccountByUsername(pvpUser.User).ConfigureAwait(false);
            if (!accountResponse.Success)
                throw new UnauthorizedAccessException("Konto konnte nicht bestimmt werden.", accountResponse.Error);
            if (accountResponse.Result.Bpk == null)
                throw new UnauthorizedAccessException("Konto nicht personengebunden.");

            return new NatürlichePerson
            {
                Bpk = accountResponse.Result.Bpk,
                EMail = new[] { new EMailadresse(accountResponse.Result.EMail) },
                Geburtsdatum = (DateTime)accountResponse.Result.Geburtstag,
                Vornamen = new[] { accountResponse.Result.Vorname },
                Nachname = accountResponse.Result.Nachname
            };
        }
        #endregion Doku_Snippet_GetPerson

        #region Doku_Snippet_SubmitAntrag
        /// <summary>
        /// Endpunkt für das übermitteln eines neuen Antrags
        /// </summary>
        [HttpPost, ActionName(nameof(SubmitAntrag))]
        public async Task<ActionResult> SubmitAntrag([FromBody] Weihnachtsdekoration antrag)
        {
            // Checks
            if (antrag == null)
                throw new ArgumentNullException(nameof(antrag));
            var person = await GetPerson().ConfigureAwait(false);

            // Antragsdaten ergänzen
            antrag.Antragsteller = person;
            antrag.Erstelldatum = antrag.Änderungsdatum = DateTime.Now;
            antrag.Id = Guid.NewGuid();
            antrag.Statustext = "In Bearbeitung";
            antrag.Verfahrensstatus = Verfahrensstatus.Laufend;

            // In unsere Datenbank hinzufügen
            ApiController.Dekos.Add(antrag);
            ApiController.Protokoll.Add((antrag.Id,
                new ReinTextVerfahrensschritt
                {
                    Erstelldatum = antrag.Erstelldatum,
                    VonBürger = true,
                    Text = "Deko beantragt"
                }));

            // An Databroker senden
            var api = _dssConnection.GetVerfahrenAPI(new DssAuthModeFis(_dssSettings));
            await api.AddVerfahren(new VerfahrenDatensatz
            {
                AntragstellerPerson = person,
                DetailsEintrag = new Eintrag { FisId = _dssSettings.Fis.Id, Name = "Weihnachtsdekoration" },
                Erstelldatum = antrag.Erstelldatum,
                FisId = _dssSettings.Fis.Id,
                Id = antrag.Id,
                Name = antrag.Name,
                Statustext = antrag.Statustext,
                VerfahrensschritteEintrag = new Eintrag { FisId = _dssSettings.Fis.Id, Name = "Ablaufsprotokoll" },
                Verfahrensstatus = antrag.Verfahrensstatus
            }).ConfigureAwait(false);

            return Ok();
        }
        #endregion Doku_Snippet_SubmitAntrag

        #region Doku_Snippet_CloseAntrag
        /// <summary>
        /// Endpunkt für das abschließen eines offenen Antrags
        /// </summary>
        [HttpPost, ActionName(nameof(CloseAntrag))]
        public async Task CloseAntrag([FromQuery] Guid id)
        {
            // Aus Datenbank abrufen
            var weihnachtsdekoration = ApiController.Dekos.Single(deko => deko.Id == id);

            // Checks
            if (weihnachtsdekoration.Verfahrensstatus != Verfahrensstatus.Laufend)
                throw new InvalidOperationException("Antrag im falschen Status.");

            // In Datenbank updaten
            weihnachtsdekoration.Verfahrensstatus = Verfahrensstatus.Abgeschlossen;
            weihnachtsdekoration.Statustext = "Genehmigt";
            weihnachtsdekoration.Änderungsdatum = DateTime.Now;
            ApiController.Protokoll.Add((id,
                new ReinTextVerfahrensschritt
                {
                    Erstelldatum = weihnachtsdekoration.Änderungsdatum,
                    Text = "Antrag wurde genehmigt.",
                    VonBürger = false
                }));

            // Verfahren am Databroker updaten
            var verfahrenApi = _dssConnection.GetVerfahrenAPI(new DssAuthModeFis(_dssSettings));
            await verfahrenApi.UpdateVerfahren(new VerfahrenDatensatz
            {
                Id = id,
                Statustext = weihnachtsdekoration.Statustext,
                Verfahrensstatus = weihnachtsdekoration.Verfahrensstatus
            }).ConfigureAwait(false);

            // Push-Benachrichtigung an alle interessierten FIS schicken
            var pushApi = _dssConnection.GetPushAPI(new DssAuthModeFis(_dssSettings));
            var datensatzupdate = new Datensatzupdate
            {
                Datensatznummer = weihnachtsdekoration.Id.ToString(),
                Eintrag = new Eintrag { FisId = _dssSettings.Fis.Id, Name = "Weihnachtsdekoration" },
                BenachrichtigungsDetails = new BenachrichtigungsDetails
                {
                    Titel = "Neue Deko: " + weihnachtsdekoration.Name,
                    Text = "Es gibt eine neue Weihnachtsdekoration in unserer Stadt!"
                },
                Updateart = Updatearten.Neu
            };
            await pushApi.PushDaten(new Datensatzupdate[] { datensatzupdate }).ConfigureAwait(false);
        }
        #endregion Doku_Snippet_CloseAntrag
    }
}