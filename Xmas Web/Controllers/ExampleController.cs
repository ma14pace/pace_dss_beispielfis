﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using MagWien.PACE.Base.AspNet.ActionFilter;

namespace MagWien.PACE.Xmas.Controllers
{
    [ApiController]
    [Route("api/[action]")]
    public class ExampleController : ControllerBase
    {
        public ExampleController(IHttpClientFactory httpClientFactory)
        {
            var client = httpClientFactory.CreateClient("Trusted");

        }

        [HttpGet]
        public void GetTest(string argument)
        {

        }
    }
}
