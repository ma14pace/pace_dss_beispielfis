﻿using MagWien.PACE.Base.Standard.Api;
using System.Diagnostics.CodeAnalysis;

namespace MagWien.PACE.Xmas
{
    #region Doku_Snippet_HttpClientSettings
    /// <summary>
    /// Infos zur Kommunikation über HTTP
    /// </summary>
    public class HttpClientSettings
    {
        /// <summary>
        /// Zu verwendende HttpClients
        /// </summary>
        [SuppressMessage("Performance", "CA1819:Properties should not return arrays", Justification = "Konfiguration")]
        public HttpClientSetting[] HttpClients { get; set; }
    }
    #endregion Doku_Snippet_HttpClientSettings
}