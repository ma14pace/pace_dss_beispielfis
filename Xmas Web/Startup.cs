using MagWien.PACE.Base.Standard.Api;
using MagWien.PACE.DSS.DatabrokerServices;
using MagWien.PACE.DSS.DatabrokerServices.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;

namespace MagWien.PACE.Xmas
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            // Eigenes DI
            #region Doku_Snippet_HttpClients
            var httpClientSettings = Configuration.GetSection("HttpClientSettings").Get<HttpClientSettings>();
            new HttpClientHandlerFactory(services).InitHttpClients(httpClientSettings.HttpClients);
            services.AddSingleton(Configuration.GetSection("DSS").Get<DssSettings>());
            #endregion Doku_Snippet_HttpClients
            #region Doku_Snippet_DssConnection
            services.AddScoped<IDssConnection, DssConnection>();
            #endregion Doku_Snippet_DssConnection

            services.AddCors(options =>
            {
                options.AddDefaultPolicy(builder =>
                    builder.SetIsOriginAllowed(_ => true).AllowAnyMethod().AllowAnyHeader().AllowCredentials());
            });

            // Controllers
            #region Doku_Snippet_Newtonsoft
            services.AddControllers().AddNewtonsoftJson(o =>
            {
                o.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                o.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                o.SerializerSettings.Converters.Add(new StringEnumConverter());
            });
            #endregion Doku_Snippet_Newtonsoft
        }

        public static void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env == null)
                throw new ArgumentNullException(nameof(env));
            if (app == null)
                throw new ArgumentNullException(nameof(app));

            // Webserver
            #region Doku_Snippet_Webserver
            app.UseRouting();
            app.UseDefaultFiles(new DefaultFilesOptions
            {
                DefaultFileNames = new string[] { "index.htm", "index.html" }
            });
            app.UseStaticFiles();   // alle anderen wwwroot-Dateien
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            #endregion Doku_Snippet_Webserver
            app.UseCors();
            app.UseDeveloperExceptionPage();

            Console.WriteLine("Startup finished");
        }
    }
}